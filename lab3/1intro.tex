\section*{1. Introdução}
Os filtros adaptativos são sistemas (contínuos ou discretos), de parâmetros variáveis no tempo, que têm um conjunto de características de adaptação, implementação e desempenho muito vantajosas face ao filtro não adaptativo. São principalmente implementáveis na forma digital devido à complexidade dos algoritmos de optimização , e daí continuarem a desenvolver-se em paralelo com a evolução da tecnologia digital (maior peso computacional em menor tempo de execução). Existem 2 modos operacionais: Correcção e Identificação - neste último caso o filtro deve evoluir para um estado em que identifique um sistema desconhecido $H(z)$ a que está associado (Figura \ref{fig:modo}). Existem diversas áreas de aplicação e neste trabalho estudaremos a de cancelamento de eco - onde $H(z)$ produz um eco ($e_k$) que queremos reproduzir no cancelador ($\widehat{e}_k$) para o anular.


A implementação do cancelador de eco (Figura \ref{fig:canecoFIR0}) utilizará um filtro FIR {\it (Finite Impulse Response)}, que por sua vez é um filtro digital com resposta finita a um impulso de entrada, em que a saída é composta por uma uma ponderação \eqref{eq:FIR} em coeficientes $h_i$ das amostras $x_{k-i}$ do sinal $x_k$ de entrada ($k$ é a iterada do sinal e $i$ a baixada que contém a amostra com $i$ períodos de atraso em relação à actual, $i$ pode ir de 0 à ordem $N$ do filtro). A saída $X_k$ do FIR será então:
\begin{equation} \label{eq:FIR}
X_k=\sum^N_{i=0} x_{k-i} \cdot h_i
  \end{equation}

\begin{figure}[H]
\centering
  \begin{subfigure}{.49\textwidth}
    \centering
    \includegraphics[width=\columnwidth]{img/esq0.png}
    \caption{Filtro adaptativo em modo de Identificação, onde 
             $\widehat{H}(z)$ se pretende igualar a $H(z)$ do 
             sistema a identificar.}
    \label{fig:modo}
  \end{subfigure}
  \begin{subfigure}{.49\textwidth}
    \centering
    \includegraphics[width=\columnwidth]{img/esq4.png}
    \caption{Cancelador de Eco implementado com Filtro Adaptativo (filtro 
     FIR retroalimentado com algoritmo LMS). Identificação de $H(z)$ na 
     estabilização dos coeficientes $c_i$ onde o $y_k=e_k-\widehat{e}_k$ 
     será mínimo.}
     \label{fig:canecoFIR0}
    \end{subfigure}
\caption{ }
\label{fig:canecoFIR}
\end{figure}

O bloco de processamento mais importante do filtro adaptativo é o algoritmo de adaptação dos seus coeficientes - do qual dependem as caracterísiticas de desempenho do filtro - e está condicionado pela viabilidade da sua implementeção.

Existem 2 famílias principais: os algoritmos de mínimos quadrados e
os algoritmos de gradiente - estes minimizam o valor quadrático médio do erro e estimam o gradiente das superfícies de nível. Destes algoritmos de gradiente é um exemplo o LMS {\it (Least Mean Squares)} que vamos aqui utilizar. Particularmente simples e robusto o LMS tem um peso computacional reduzido e em conjunto com um filtro FIR forma um conjunto estável e de velocidade de convergência moderada.

Este algoritmo, também chamado {\it de Algoritmo de Gradiente Estocástico}, resulta de uma simplificação do {\it Algoritmo do Gradiente Determinístico} onde, no cálculo do gradiente se toma o valor instantâneo do erro quadrático, $y_k^2$, como estimativa do seu valor médio estatístico ou valor esperado $E[y_k^2]$. De acordo com o LMS, a actualização dos coeficientes do filtro FIR em cada iteração $c_{i,k} \rightarrow c_{i,k+1}$, é dada em função do passo $\mu$, erro instantâneo $y_k$ e amostra do sinal $x_{k-i}$ (baixada $i$ iteração $k$):

\begin{equation} \label{eq:actualiza}
  c_{i,k+1}= c_{i,k} + 2 \mu y_k x_{k-i}
\end{equation}

Por fim, a capacidade do sistema de corrigir o eco é, usualmente, dada pelo ERLE {\it (Echo Return Loss Enhancement)}. Quanto melhor o sistema for a cancelar o eco, menor a diferença $e_k-\widehat{e}_k$ (ou o erro $y_k$) e maior o valor de ERLE.
\begin{equation} \label{eq:ERLE}
  ERLE=\frac{E[e_k^2]}{E[(e_k-\widehat{e}_k)^2]} \Bigl.\Bigr|_{dB}=\frac{E[d_k^2]}{E[y_k^2]} \Bigl.\Bigr|_{dB}=10log_{10}\Bigl|\frac{E[d_k^2]}{E[y_k^2]} \Bigl.\Bigr|
  \end{equation}
