# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'T1_150.tex'

# setting the axis ranges
set grid mxtics ytics xtics
set xrange[100:100000]
set yrange[-45:2]
set logscale x

set format x '$%.0l\times10^{%L}$'
#set mxtics 20
#set xtics 100
set xlabel '$f(Hz)$'
set ylabel 'Ganhos $(dB)$'

k=1.0
Q=1.0
w=1.0/(1e4*4.7e-9)/2/pi

#defining a function
T1(x)=20.0*log10(x*x*k/sqrt((w*w-x*x)**2+w*w*x*x))
T2(x)=20.0*log10(x*w*k/sqrt((w*w-x*x)**2+w*w*x*x))
T3(x)=20.0*log10(w*w*k/sqrt((w*w-x*x)**2+w*w*x*x))
set dummy x	#setting the dummy variable


# setting the key (plot labels)
#set nokey   	       #uncomment to hide the label
set key box width -1 bottom center

#defining a line style
set style line 1 lc rgb'#0000CC' lt 1 lw 2
set style line 2 lc rgb'#00CC00' lt 1 lw 2
set style line 3 lc rgb'#CC0000' lt 1 lw 2


plot T1(x) title "Ganho $T_1$" ls 1, T2(x) title "Ganho $T_2$" ls 2, T3(x) title "Ganho $T_3$" ls 3

