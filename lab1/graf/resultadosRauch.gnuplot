# select the output
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output "resultadosRauch.tex"

set dummy x	#setting the dummy variable


set grid mxtics ytics xtics
set xrange[100:50000]
set yrange[-10:25]
set logscale x
set ytics 5
set format x '$%.0l\times10^{%L}$'
#set mxtics 20
#set xtics 100
set xlabel '$f (Hz)$'
set ylabel 'Ganhos $(dB)$'

#z(x)=x*2*pi
w=1000.0
B=250.0
Q=w/B
#k=10**(6.0/5.0)
k=15.8489
#defining a function
#S(x)=(x**2+w**2)/(B*x)
#T(x)=20.0*log10(k*B*x/(x**2+w**2+B*x))

T(x)=20.0*log10(x*w*k/Q/sqrt((w*w-x*x)**2+w/Q*w/Q*x*x))


# setting the key (plot labels)
#set nokey   	       #uncomment to hide the label
set key top right box 2	       #draw a box arround the label

#defining a line style
set style line 1 lc rgb'#0000CC' lt 1 lw 1
set style line 4 lc rgb'#0000CC' lt 1 lw 1.5

set style line 2 lc rgb'#00CC00' lt 1 lw 1
set style line 5 lc rgb'#00CC00' lt 2 lw 1

set style line 3 lc rgb'#CC0000' lt 1 lw 1
set style line 6 lc rgb'#CC0000' lt 3 lw 1


plot "dadosRauch.txt" us ($1*1):2 title 'Exp' ls 4, T(x) title 'Teorico'
