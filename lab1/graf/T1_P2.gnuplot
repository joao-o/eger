#comments started with '#'
# consider we also want to plot the best fitting function to our points

reset
set encoding utf8
#epslatex

#set terminal x11

set terminal gif enhanced medium animate delay 4
set output "anim.gif"

# setting the axis ranges
set xrange[100:100000]
set yrange[-35:5]
set logscale x

set grid xtics mxtics ytics
set key left
set key Left

r5=1e5
r2=1e5
r3=1e4
p2=1e4

w=1.0/(1e4*4.7e-9)/(2*pi)
kvi(k)=r3/p2/(k*(1-k))+1
kv2(k)=p2/r3*k+k/(1-k)+1
c=(r5+r2)/r2

#defining a function
T1(x,k)=20*log10(c/kvi(k)*x*x/sqrt((r5/r2*w*w-x*x)**2+(c/kv2(k)*w*x)**2))
set dummy x	#setting the dummy variable

do for [k=0:100] {
  plot T1(x,(k+1.0-1.0)/100) notitle
}
do for [k=100:0:-1] {
  plot T1(x,(k+1.0-1.0)/100) notitle
}

set format x '$%.0l\times10^{%L}$'
set xlabel '$f(Hz)$'
set ylabel '$T_1(dB)$'

set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'T1_P2.tex'

set key bottom right box 4

#defining a line style
set style line 1 lc rgb'#0000CC' lt 1 lw 2
set style line 2 lc rgb'#00CC00' lt 1 lw 2
set style line 3 lc rgb'#CC0000' lt 1 lw 2
set style line 4 lc rgb'#CC00CC' lt 1 lw 2
set style line 5 lc rgb'#CCCC00' lt 1 lw 2
set style line 6 lc rgb'#00CCCC' lt 1 lw 2

plot T1(x,0.2) title '$\alpha=0.2$' ls 1,\
     T1(x,0.4) title '$\alpha=0.4$' ls 2,\
     T1(x,0.5) title '$\alpha=0.5$' ls 3,\
     T1(x,0.6) title '$\alpha=0.6$' ls 4,\
     T1(x,0.8) title '$\alpha=0.8$' ls 5
