# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'T1_150_fase.tex'

set angles degrees

# setting the axis ranges
set xrange[100:100000]
set yrange[-185:185]
set logscale x

set grid mxtics xtics ytics
set format x '$10^{%L}$'
set ytics 45
set xlabel '$f(Hz)$'
set ylabel 'Fase ($ ^o$)'

p2=1.0e4
r2=1.0e5
r4=1.0e4
r6=1.0e4
r5=1.0e5
r11=1.0e4
c1=4.7e-9
c2=4.7e-9

k=sqrt(r4*r2*r6*c1/(p2*p2*r5*c2));
w=sqrt(r5/(r2*r11*r6*c1*c2))/(2*pi)
Q=sqrt(r6*r6*r5*c1/(r11*r2*r4*c2))

print w*2*pi

#defining a function
t1(x)=atan((w*w-x*x)/(w*x/Q))+90
t2(x)=  x<w ? (atan((w*w-x*x)/(w*x/Q))-180) : 1/0
t2_(x)= x>w ? (atan((w*w-x*x)/(w*x/Q))+180) : 1/0
t3(x)=atan((w*w-x*x)/(w*x/Q))-90
set dummy x	#setting the dummy variable

set key box width -2.4 at 2500,110

#defining a line style
set style line 1 lc rgb'#0000CC' lt 1 lw 2
set style line 2 lc rgb'#00CC00' lt 1 lw 2
set style line 3 lc rgb'#CC0000' lt 1 lw 2


plot t1(x) title "Fase  $T_1$" ls 1,\
     t2(x) title "Fase  $T_2$" ls 2,\
     t2_(x) notitle ls 2,\
     t3(x) title "Fase  $T_3$" ls 3

