# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'T1_160.tex'

# setting the axis ranges
set xrange[100:100000]
set yrange[-45:2]
set logscale x

set format x '$10^{%L}$'
set xlabel '$f(Hz)$'
set ylabel 'Ganhos $(dB)$'
set grid xtics ytics mxtics

p2=1.0e4
r2=1.0e5
r4=1.0e4
r6=1.0e4
r5=1.0e5
r11=1.0e4
c1=4.7e-9
c2=4.7e-9

k=sqrt(r4*r2*r6*c1/(p2*p2*r5*c2));
w=sqrt(r5/(r2*r11*r6*c1*c2))/(2*pi)
Q=sqrt(r6*r6*r5*c1/(r11*r2*r4*c2))

#defining a function
f(x)=x*k*w/sqrt((w*w-x*x)**2+(w*x/Q)**2)
T1(x)=20.0*log10(f(x))
T2(x)=20.0*log10(k*w*w/sqrt((w*w-x*x)**2+(w*x/Q)**2))
set dummy x	#setting the dummy variable


set key box width -1 bottom center

set style line 1 lc rgb'#0000CC' lt 1 lw 2
set style line 2 lc rgb'#00CC00' lt 1 lw 2
set style line 3 lc rgb'#CC0000' lt 1 lw 2


plot T1(x) title "Ganho $T_1$" ls 1, T2(x) title "Ganho $T_2$" ls 3

