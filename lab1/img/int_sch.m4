
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
 dot;
 {
   move left 0.1
   "$v_i$"
 }
 resistor(right_ 0.6);
 dot;
 Fork: Here
 line right 0.2
 A1:opamp() with .In1 at Here
 line from Fork up 0.3 
 capacitor(right_ 0.82)
 line to A1.Out
 dot
 line right 0.3
 dot
 move right 0.1
 "$v_o$"
 line from A1.In2 left 0.2
 ground

.PE
