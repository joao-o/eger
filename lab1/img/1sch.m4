
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
 dot
 resistor(right_ 0.6)
 dot
 {
   line right 0.2
   A1:opamp() with .In1 at Here
 }
 line up 0.3 
 capacitor(right_ 0.82)
 line to A1.Out
 dot
 line right 0.3
 dot
 line from A1.In2 left 0.2
 ground

.PE
