% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
"$v_+$"
move right 0.15
dot
line right 0.2
P2:potentiometer(up_ 0.5) with .T1 at Here; rlabel(,"$P_2$",)
move to P2.Start
ground 
move to P2.End
line left 0.41
dot
move left 0.15
"$v_2$"
move to P2.T1
move right 0.5
arrow right 0.4
move right 0.2
"$v_+$"
move right 0.15 
dot
line right 0.4
dot
{
  resistor(down_ 0.4);llabel(,"$(1-\alpha)P_2$",)
  ground
}
resistor(up_ 0.4);rlabel(,"$\alpha P_2$",)
line left 0.4
dot
move left 0.15
"$v_2$"
.PE
