
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
 
 "$v_i$"
 move right 0.1
 dot
 resistor(right 0.5);variable;llabel(,"$P_2$",)
 dot;
 line right 0.1
 A1:opamp() with .In1 at Here
 line from A1.In2 left 0.1
 ground()
 line from A1.In1 left 0.1
 line up 0.3 
 {
   dot
   capacitor(right_ 0.72);rlabel(,"$C_1$",)
   dot
 }
 line up 0.2
 Loop: Here
 resistor(right_ 0.72);llabel(,"$R_6$",)
 line to A1.Out
 dot
 {
   line down 0.2
   line right 0.1
   dot
   move right 0.1
   "$v_1$"
 }
 resistor(right_ 0.5);llabel(,"$R_{11}$",)
 dot;
 {
   line right 0.1
   A2:opamp() with .In1 at Here
   line from A2.In2 left 0.1
   ground()
 }
 line up 0.3 
 capacitor(right_ 0.72);llabel(,"$C_2$",)
 line to A2.Out
 dot
 {
   line down 0.3
   line right 0.1
   dot
   move right 0.1
   "$v_2$"
 }
 resistor(right_ 0.5);llabel(,"$R_2$",)
 dot;
 {
   line right 0.1
   A3:opamp() with .In1 at Here
   line from A3.In2 left 0.1
   ground()
 }
 line up 0.3 
 resistor(right_ 0.72);llabel(,"$R_5$",)
 dot
 {
   line up 0.7
   line left 1
   resistor(left_ 0.5);llabel(,"$R_4$",)
   line left 1.67
   line to Loop
   dot
 }
 line to A3.Out
 dot
 line right 0.1
 dot
 move right 0.1
 "$v_o$"

.PE
