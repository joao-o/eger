
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
 
 "$v_i$"
 move right 0.1
 dot
 resistor(right 0.5);llabel(,"$R_3$",)
 dot;
 {
   line right 0.1
   A1:opamp() with .In2 at Here
 }
 line from A1.In1 left 0.1
 line up 0.3 
 Loop: Here
 resistor(right_ 0.72);llabel(,"$R_5$",)
 line to A1.Out
 dot
 {
   line down 0.2
   line right 0.1
   dot
   move right 0.1
   "$v_1$"
 }
 resistor(right_ 0.5);llabel(,"$R_6$",)
 dot;
 {
   line right 0.1
   A2:opamp() with .In1 at Here
   line from A2.In2 left 0.1
   ground()
 }
 line up 0.3 
 capacitor(right_ 0.72);llabel(,"$C_1$",)
 line to A2.Out
 dot
 {
   line down 0.4
   dot
   { 
     line right 0.1
     dot
     move right 0.1
     "$v_2$"
   }
   line left 1
   P2:potentiometer(left_ 0.4) with .Start at Here; rlabel(,"$P_2$",)
   line from P2.End left 0.55 
   line up 0.4
 }
 resistor(right_ 0.5);llabel(,"$R_{11}$",)
 dot;
 {
   line right 0.1
   A3:opamp() with .In1 at Here
   line from A3.In2 left 0.1
   ground()
 }
 line up 0.3 
 capacitor(right_ 0.72);llabel(,"$C_2$",)
 dot
 {
   line up 0.5
   line left 1
   resistor(left_ 0.5);llabel(,"$R_2$",)
   line left 1.67
   line to Loop
   dot
 }
 line to A3.Out
 dot
 line right 0.1
 dot
 move right 0.1
 "$v_o$"

.PE
