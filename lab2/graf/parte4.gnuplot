# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'parte4.tex'

#defining a line style
set style line 1 lc rgb'#0000FF' lt 1 lw 2
set style line 2 lc rgb'#00FF00' lt 1 lw 2
set style line 3 lc rgb'#FF0000' lt 1 lw 2
set style line 4 lc rgb'#AAAA00' lt 1 lw 2
set style line 5 lc rgb'#FFFF00' lt 1 lw 2
set style line 6 lc rgb'#FF00FF' lt 1 lw 2
set style line 7 lc rgb'#FF2222' lt 1 lw 4
set style line 8 lc rgb'#FF6666' lt 1 lw 4

set grid mxtics ytics xtics
set xrange[-0.5:16.5] 
set yrange[-6.7:0.5]
set mxtics 2

set xlabel '$Q_i$'
set ylabel '$V_o (V)$'
set samples 1000

Rref=1
Vref=5
CLK(x)=(2*floor(x)-floor(2*x)+1)

s1(x)=-1.0+1.0*(2*-floor(x*2**-1)+floor(2*x*2**-1))
s2(x)=-1.0+1.0*(2*-floor(x*2**-2)+floor(2*x*2**-2))
s3(x)=-1.0+1.0*(2*-floor(x*2**-3)+floor(2*x*2**-3))
s4(x)=-1.0+1.0*(2*-floor(x*2**-4)+floor(2*x*2**-4))
tot(x,R)=R/3*(s1(x)/16.0+s2(x)/8.0+s3(x)/4.0+s4(x)/2.0)*5

set dummy x	#setting the dummy variable
# setting the key (plot labels)
#set nokey   	       #uncomment to hide the label
set key bottom center width 1

plot tot(x,2.0) title '$R_f=2R$' ls 7,\
     tot(x,4.0) title '$R_f=4R$' ls 8
