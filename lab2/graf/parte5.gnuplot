#defining a line style
set style line 1 lc rgb'#0000FF' lt 1 lw 2
set style line 2 lc rgb'#00FF00' lt 1 lw 2
set style line 3 lc rgb'#FF0000' lt 1 lw 2
set style line 4 lc rgb'#AAAA00' lt 1 lw 2
set style line 5 lc rgb'#FFFF00' lt 1 lw 2
set style line 6 lc rgb'#FF00FF' lt 1 lw 2
set style line 7 lc rgb'#FF2222' lt 1 lw 2
set style line 8 lc rgb'#FF6666' lt 1 lw 2

set grid mxtics ytics xtics
set xrange[16.5:-0.5] 
set yrange[-7.7:0.5]
set mxtics 2

set xlabel '$Q_i$'
set ylabel '$V_o (V)$'
set samples 1000

Rref=1
Vref=5

s1(x)=-1.0*(2*-floor(x*2**-1)+floor(2*x*2**-1))
s2(x)=-1.0*(2*-floor(x*2**-2)+floor(2*x*2**-2))
s3(x)=-1.0*(2*-floor(x*2**-3)+floor(2*x*2**-3))
s4(x)=-1.0*(2*-floor(x*2**-4)+floor(2*x*2**-4))
tot(x,R)=R/3*(s1(x)*1.0/16+s2(x)*2.0/16+s3(x)*4.0/16+s4(x)*8.0/16)*5

v1(x)=(84.0/128*s4(x)+21.0/64*s3(x)+5.0/32*s2(x)+01.0/8*s1(x))*5
v2(x)=(021.0/32*s4(x)+05.0/16*s3(x)+01.0/4*s2(x)+1.0/16*s1(x))*5
v3(x)=(0005.0/8*s4(x)+001.0/2*s3(x)+01.0/8*s2(x)+1.0/16*s1(x))*5
v4(x)=(0001.0/1*s4(x)+001.0/4*s3(x)+01.0/8*s2(x)+1.0/16*s1(x))*5

set dummy x	#setting the dummy variable

set key  bottom right
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'parte5-1.tex'

plot v1(x) title "$R_1=R$" ls 1,\
     v2(x) title "$R_2=R$" ls 3

set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'parte5-2.tex'

plot v3(x) title "$R_3=R$" ls 1,\
     v4(x) title "$R_4=R$" ls 3
