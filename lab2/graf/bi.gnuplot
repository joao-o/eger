# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'bi.tex'

#defining a line style
set style line 1 lc rgb'#0000FF' lt 1 lw 2
set style line 2 lc rgb'#00FF00' lt 1 lw 2
set style line 3 lc rgb'#FF0000' lt 1 lw 2
set style line 4 lc rgb'#AAAA00' lt 1 lw 2
set style line 5 lc rgb'#FFFF00' lt 1 lw 2
set style line 6 lc rgb'#FF00FF' lt 1 lw 2
set style line 7 lc rgb'#AAAAAA' lt 1 lw 4

# setting the axis ranges
set grid mxtics ytics xtics
set xrange[0:17]
set yrange[0:5]

#set mxtics 20
#set xtics 100
set xlabel 't(s)'
set ylabel '$V_o$'

#defining a function
b1(x)=((x>1 && x<2)||(x>3 && x<4)||(x>5 && x<6)||(x>7 && x<8)||(x>9 && x<10)||(x>11 && x<12)||(x>13 && x<14)||(x>15 && x<16)) ? 1.0 : 0;
b2(x)=((x>2 && x<4)||(x>6 && x<8)||(x>10 && x<12)||(x>14 && x<16)) ? 1.0 : 0;
b3(x)=((x>4 && x<8)||(x>12 && x<16)) ? 1.0 : 0;
b4(x)=(x>8 && x<16) ? 1.0 : 0;

Rref=1
Vref=5
#V(x)=1.0*b1(x)+2.0*b2(x)+4.0*b3(x)+8.0*b4(x)
V0(x)=(b1(x)+2*b2(x)+4*b3(x)+8*b4(x))*Rref*Vref/24;
V1(x)=(16*b1(x)+20*b2(x)+42*b3(x)+85*b4(x))*Rref*Vref/256;
V2(x)=(2*b1(x)+8*b2(x)+10*b3(x)+21*b4(x))*Rref*Vref/64;
V3(x)=(1*b1(x)+2*b2(x)+8*b3(x)+10*b4(x))*Rref*Vref/32;
V4(x)=(1*b1(x)+2*b2(x)+4*b3(x)+16*b4(x))*Rref*Vref/32;

set dummy x	#setting the dummy variable

# setting the key (plot labels)
#set nokey   	       #uncomment to hide the label
set key left top



#plot V0(x) title 'V' ls 1
plot V0(x) title '$V_O$' ls 7, V1(x) title '$V_{O1}$' ls 1, V2(x) title '$V_{O2}$' ls 2, V3(x) title '$V_{O3}$' ls 3, V4(x) title '$V_{O4}$' ls 4 

