

% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
  ground
  resistor(up_ 0.4);llabel(,"$2R$",)
  line right 0.3
  {
    resistor(down_ 0.4);llabel(,"$R_1$",)
    dot;
    move down 0.1
    "$S_1$"
  }
  dot
  {
    move up 0.1
    "$v_1$"
  }
  resistor(right_ 0.4);llabel(,"$R$",)
  {
    resistor(down_ 0.4);llabel(,"$R_2$",)
    dot;
    move down 0.1
    "$S_2$"
  }
  dot
  {
    move up 0.1
    "$v_2$"
  }
  resistor(right_ 0.4);llabel(,"$R$",)
  {
    resistor(down_ 0.4);llabel(,"$R_3$",)
    dot;
    move down 0.1
    "$S_3$"
  }
  dot
  {
    move up 0.1
    "$v_3$"
  }
  resistor(right_ 0.4);llabel(,"$R$",)
  {
    resistor(down_ 0.4);llabel(,"$R_4$",)
    dot;
    move down 0.1
    "$S_4$"
  }
  dot
  {
    move up 0.1
    "$v_4$"
  }
  resistor(right_ 0.4);llabel(,"$2R$",)
  dot
  Fork: Here
  line right 0.2
  A1:opamp() with .In1 at Here
  line from Fork up 0.3 
  resistor(right_ 0.82);llabel(,"$R_f$",)
  line to A1.Out
  dot
  line right 0.3
  dot
  move right 0.1
  "$v_o$"
  line from A1.In2 left 0.2
  ground

.PE
